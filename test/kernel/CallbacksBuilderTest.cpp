#include <functional>
#include "../catch.h"
#include "../../src/kernel/CallbacksBuilder.h"

bool logger_called = false;
bool allocator_called = false;
bool liberator_called = false;
bool step_finished_called = false;

void TestLogger(fmi2ComponentEnvironment env, fmi2String instance_name, fmi2Status status, fmi2String category, fmi2String message, ...) {
	logger_called = true;
};

void* TestAllocator(size_t number_of_objects, size_t size) {
	allocator_called = true;
	return nullptr;
}

void TestLiberator(void* object) {
	liberator_called = true;
}

void TestStepFinished(fmi2ComponentEnvironment environment, fmi2Status status) {
	step_finished_called = true;
}

TEST_CASE("CallbacksBuilderTest") {
	SECTION("default callbacks has no stepFinished callbacks") {
		auto functions = CallbacksBuilder().DefaultCallbacks();
		REQUIRE(functions->logger != nullptr);
		REQUIRE(functions->allocateMemory != nullptr);
		REQUIRE(functions->freeMemory != nullptr);
		REQUIRE(functions->stepFinished == nullptr);
		REQUIRE(functions->componentEnvironment == nullptr);
	}

	SECTION("default callbacks can be overwritten") {
		logger_called = false;
		allocator_called = false;
		liberator_called = false;
		step_finished_called = false;
		auto functions = CallbacksBuilder()
			.Configure()
			.Logger(TestLogger)
			.AllocateMemory(TestAllocator)
			.FreeMemory(TestLiberator)
			.StepFinished(TestStepFinished)
			.Build();
		functions->logger(nullptr, nullptr, fmi2OK, nullptr, "");
		functions->allocateMemory(0, 0);
		functions->freeMemory(nullptr);
		functions->stepFinished(nullptr, fmi2OK);
		REQUIRE(logger_called == true);
		REQUIRE(allocator_called == true);
		REQUIRE(liberator_called == true);
		REQUIRE(step_finished_called == true);
	}
}

