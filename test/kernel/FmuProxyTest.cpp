#include "../../src/modeldescription/v2/ModelDescription.h"
#include "../catch.h"
#include "../hippomocks.h"
#include "../../src/kernel/FmiLibrary.h"
#include "../../src/kernel/v2/FmuProxy.h"

using std::unique_ptr;

TEST_CASE("FmuProxyTest") {
	MockRepository mocks;
	shared_ptr<FmiLibrary> library(mocks.Mock<FmiLibrary>());
	FmuProxy proxy(library);
	auto model_identifier("aModelID");
	auto guid("aGuID");
	shared_ptr<fmi2Component> component(new fmi2Component());

	mocks.ExpectCall(library.get(), FmiLibrary::Instantiate).Return(component);

	proxy.Instantiate(model_identifier, guid);

	SECTION("creation, destruction and logging of instances") {

		mocks.ExpectCall(library.get(), FmiLibrary::SetDebugLogging).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::FreeInstance);

		vector<string> categories{ "ERROR", "DEBUG", "INFO" };
		proxy.SetDebugLogging(true, categories);
		proxy.FreeInstance();
	}

	SECTION("initialization, termination, and resetting") {
		auto tolerance_defined = false;
		auto tolerance = 1e-6;
		auto start_time = 0.;
		auto stop_time_defined = false;
		auto stop_time = 1.;

		mocks.ExpectCall(library.get(), FmiLibrary::SetupExperiment).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::EnterInitializationMode).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::ExitInitializationMode).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::Reset).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::Terminate).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::FreeInstance);

		REQUIRE(proxy.SetupExperiment(tolerance_defined, tolerance, start_time, stop_time_defined, stop_time) == Status::OK);
		REQUIRE(proxy.EnterInitMode() == Status::OK);
		REQUIRE(proxy.ExitInitMode() == Status::OK);
		REQUIRE(proxy.Reset() == Status::OK);
		REQUIRE(proxy.Terminate() == Status::OK);
		proxy.FreeInstance();
	}

	SECTION("getting and setting variable values") {
		vector<unsigned> value_references{ 1, 2, 3 };
		mocks.ExpectCall(library.get(), FmiLibrary::GetReal).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::GetInteger).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::GetBoolean).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::GetString)
			.Do([](void*, const unsigned[], size_t number_of_value_references, char* output[]) -> int {
				for (auto i = 0; i < number_of_value_references; i++) output[i] = "foo";
				return 0; }
			);

		auto reals = proxy.GetReal(value_references);
		auto integers = proxy.GetInteger(value_references);
		auto booleans = proxy.GetBoolean(value_references);
		auto strings = proxy.GetString(value_references);

		mocks.ExpectCall(library.get(), FmiLibrary::SetReal).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::SetInteger).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::SetBoolean).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::SetString).Return(0);

		REQUIRE(proxy.SetReal(value_references, reals) == Status::OK);
		REQUIRE(proxy.SetInteger(value_references, integers) == Status::OK);
		REQUIRE(proxy.SetBoolean(value_references, booleans) == Status::OK);
		REQUIRE(proxy.SetString(value_references, strings) == Status::OK);
	}

	SECTION("getting and setting the complete fmu state") {
		auto empty_state = State::NewEmptyState();
		int state_size = 10;
		mocks.ExpectCall(library.get(), FmiLibrary::GetFmuState).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::SerializedFMUstateSize)
			.Do([state_size](void*, void*, size_t* size) { *size = state_size; return 0; });
		mocks.ExpectCall(library.get(), FmiLibrary::SerializeState).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::DesrializeState).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::SetFmuState).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::FreeFmuState).Return(0);

		State state = proxy.GetFmuState(empty_state);
		proxy.DeserializeFmuState(proxy.SerializeFmuState(state, proxy.SerializedFmuStateSize(state)));
		REQUIRE(proxy.SetFmuState(state) == Status::OK);
		REQUIRE(proxy.FreeFmuState(state) == Status::OK);
	}

	SECTION("getting partial derivatives") {
		vector<unsigned> unknowns{ 1, 2, 3 };
		vector<unsigned> knowns{ 10, 20, 30 };
		vector<double> known_derivatives{ 1., 2., 3. };

		mocks.ExpectCall(library.get(), FmiLibrary::GetDirectionalDerivative).Return(0);
		vector<double> unknown_derivatives = proxy.GetDirectionalDerivative(unknowns, knowns, known_derivatives);
	}

	SECTION("cosimulation computation") {
		double communication_point = 0.;
		double step_size = 1.;
		mocks.ExpectCall(library.get(), FmiLibrary::DoStep).With(_, communication_point, step_size).Return(0);
		mocks.ExpectCall(library.get(), FmiLibrary::CancelStep).Return(0);

		REQUIRE(proxy.DoStep(communication_point, step_size) == Status::OK);
		REQUIRE(proxy.CancelStep() == Status::OK);
	}

	mocks.ExpectCallDestructor(library.get());
}