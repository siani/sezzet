#include "../../catch.h"
#include <string>
#include "../../../src/kernel/operatingsystem/SevenZipUnzipper.h"
#include <fstream>
#include <ios>
#include <exception>
#include <boost/crc.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
void RemoveTemporalDirectories();

using std::string;
using std::ifstream;

string project_path("C:/cygwin64/home/jcortes/sezzet/");
string zip_file_path(project_path + "resources/win_32_bouncingBall.fmu");
string output_folder_path(project_path + "resources/win_32_bouncingBall_out/");
const int kBufferSize = 1024;

void RemoveTemporalDirectories() {
	boost::filesystem::path ofp_path(output_folder_path);
	boost::filesystem::remove_all(ofp_path);
}

void ProcessBytes(ifstream &input_file_stream, boost::crc_32_type &result) {
	char buffer[kBufferSize];
	input_file_stream.read(buffer, kBufferSize);
	result.process_bytes(buffer, input_file_stream.gcount());
}

string ChecksumAsHexadecimalString(boost::crc_32_type &result) {
	std::stringstream stream;
	stream << std::hex << result.checksum();
	return string(stream.str());
}

int GetCrc32FromFileOrDie(string file_path) {
	ifstream input_file_stream(file_path, std::ios_base::binary);
	boost::crc_32_type result;
	if (!input_file_stream) throw std::exception("Could not open file");
	do { ProcessBytes(input_file_stream, result); } while (input_file_stream);
	return result.checksum();
}

TEST_CASE("SevenZipUnzipperTest") {
	SECTION("checksum after extraction is correct") {
		SevenZipUnzipper unzipper(zip_file_path);
		unzipper.ExtractTo(output_folder_path);
		REQUIRE(GetCrc32FromFileOrDie(output_folder_path + "\\modelDescription.xml") == 4035518048);
		RemoveTemporalDirectories();
	}
}