#include <boost/algorithm/string/predicate.hpp>
#include "../../catch.h"
#include "../../../src/kernel/operatingsystem/Fmu.h"
#include <boost/filesystem.hpp>

using namespace boost::filesystem;

TEST_CASE("FmuTest") {
	string fmu_path = "C:/cygwin64/home/jcortes/sezzet/test/resources/Car.fmu";
	string prefix = "fmu_";
	string model_name = "Car";
	string sufix = ".fmu";
	string temp_dir = temp_directory_path().generic_string();
	string os = "win";
	string arch = "64";
	string path_to_extracted_fmu = temp_dir + prefix + model_name + sufix;
	Fmu fmu(fmu_path);

	SECTION("create an Fmu implies creation of a temp dir with binaries, resources and modelDescription.xml") {

		auto model_description = fmu.ModelDescription();
		REQUIRE(boost::filesystem::exists(status(path_to_extracted_fmu)));
		REQUIRE(boost::filesystem::exists(status(path_to_extracted_fmu + "/binaries")));
		REQUIRE(boost::filesystem::exists(status(path_to_extracted_fmu + "/resources")));
		REQUIRE(boost::filesystem::exists(status(path_to_extracted_fmu + "/modelDescription.xml")));

		REQUIRE(fmu.LibraryPath(os + arch, model_name) == path_to_extracted_fmu + "/binaries/" + os + arch + "/" + model_name + ".dll");

		REQUIRE(model_description->model_name() == "Car");
		REQUIRE(model_description->GetCoSimulation().GetModelIdentifier() == "Car");
	}

	remove_all(path(fmu.WorkingDirectoryPath()));
}