#include <string>
#include "../catch.h"
#include "../../src/kernel/WindowsFmiLibrary.h"
#include "../../src/kernel/v2/FmuProxy.h"

using std::string;
using std::shared_ptr;
TEST_CASE("load a windows fmu proxy") {

	string library_path = "C:/cygwin64/home/jcortes/sezzet/test/resources/simplefmuWindows64.dll";
	string model_identifier = "name";
	string guid = "guid";
	FmuProxy proxy{ (std::make_shared<WindowsFmiLibrary>(library_path)) };
	proxy.Instantiate(model_identifier, guid);

	SECTION("a call to fmi2SetDebugLogging should return OK") {
		bool logging_on = false;
		vector<string> categories = { "DEBUG", "INFO" };
		REQUIRE(proxy.SetDebugLogging(logging_on, categories) == Status::OK);
	}

	SECTION("calls to initialization, destruction and reset should return OK") {
		bool tolerance_defined = false;
		double tolerance = 1e-5;
		double start_time = 0.;
		bool stop_time_defined = false;
		double stop_time = 1.;
		REQUIRE(proxy.SetupExperiment(tolerance_defined, tolerance, start_time, stop_time_defined, stop_time) == Status::OK);
		REQUIRE(proxy.EnterInitMode() == Status::OK);
		REQUIRE(proxy.ExitInitMode() == Status::OK);
		REQUIRE(proxy.Reset() == Status::OK);
		REQUIRE(proxy.Terminate() == Status::OK);
	}

	SECTION("GetReal reads values setted by SetReal") {
		vector<unsigned> value_references{ 10, 11 };
		vector<double> values{ 3.14, 2.71 };
		proxy.SetReal(value_references, values);
		auto output = proxy.GetReal(value_references);
		REQUIRE(output.size() == 2);
		REQUIRE(output[0] == values[0]);
		REQUIRE(output[1] == values[1]);
	}

	SECTION("GetInteger reads values setted by SetReal") {
		vector<unsigned> value_references{ 20, 21 };
		vector<int> values{ 9, 89 };
		proxy.SetInteger(value_references, values);
		auto returned_values = proxy.GetInteger(value_references);
		REQUIRE(returned_values.size() == 2);
		REQUIRE(returned_values[0] == values[0]);
		REQUIRE(returned_values[1] == values[1]);
	}

	SECTION("GetBoolean reads values setted by SetBoolean") {
		vector<unsigned> value_references{ 30, 31 };
		vector<bool> values{ false, true };
		proxy.SetBoolean(value_references, values);
		auto returned_values = proxy.GetBoolean(value_references);
		REQUIRE(returned_values.size() == 2);
		REQUIRE(returned_values[0] == values[0]);
		REQUIRE(returned_values[1] == values[1]);
	}

	SECTION("GetString reads values setted by SetString") {
		vector<unsigned> value_references{ 40, 41, 42 };
		vector<string> values{ "foo", "bar", "foobar" };
		proxy.SetString(value_references, values);
		auto returned_values = proxy.GetString(value_references);
		REQUIRE(returned_values.size() == 3);
		REQUIRE(returned_values[0] == values[0]);
		REQUIRE(returned_values[1] == values[1]);
		REQUIRE(returned_values[2] == values[2]);
	}

	proxy.FreeInstance();
}