#include "../../src/kernel/Status.h"
#include "../catch.h"
#include <string>
#include "../../src/wrapper/Simulation.h"
#include <unordered_set>

using std::string;
using std::unordered_set;

TEST_CASE("package building cases") {
	const string PATH = "C:/cygwin64/home/jcortes/sezzet/test/resources/";
	const string OUTDOOR = "PackageBuildingCases_Livraison4_01_IndoorHeatGains.fmu";
	const string INDOOR = "PackageBuildingCases_Livraison4_01_OutdoorAir.fmu";
	const string WALL1 = "PackageBuildingCases_Livraison4_01_wallFmu1.fmu";
	const string WALL2 = "PackageBuildingCases_Livraison4_01_wallFmu2.fmu";
	const string WALL3 = "PackageBuildingCases_Livraison4_01_wallFmu3.fmu";
	const string ZONE1 = "PackageBuildingCases_Livraison4_01_zoneFmu1.fmu";
	const string ZONE2BIS = "PackageBuildingCases_Livraison4_01_zoneFmu2bis.fmu";
	const string ZONE3BIS = "PackageBuildingCases_Livraison4_01_zoneFmu3bis.fmu";
	const string ZONE4 = "PackageBuildingCases_Livraison4_01_zoneFmu4.fmu";


	SECTION("creates a simulation object from an .fmu file") {
		double start_time = 0.;
		double stop_time = 1.;
		Simulation simulation(Fmu(PATH + ZONE4));
		Status init_status = simulation.Init(start_time, stop_time);
		simulation.Write("Text").With(15.);
		REQUIRE(simulation.Read("Text").AsDouble()[0] == 15.);
		REQUIRE(simulation.Read("zone.m").AsInteger()[0] == 10);

		Status terminate_status = simulation.Terminate();
		REQUIRE(init_status == Status::OK);
		REQUIRE(terminate_status == Status::OK);
	}

	SECTION("building cases complete test") {
		double start_time = 0.;
		double stop_time = 10000.;

		Simulation OutdoorModel(Fmu(PATH + OUTDOOR));
		Simulation IndoorModel(Fmu(PATH + INDOOR));
		Simulation Wall1Model(Fmu(PATH + WALL1));
		Simulation Wall2Model(Fmu(PATH + WALL2));
		Simulation Wall3Model(Fmu(PATH + WALL3));
		Simulation Zone1Model(Fmu(PATH + ZONE1));
		Simulation Zone2bisModel(Fmu(PATH + ZONE2BIS));
		Simulation Zone3bisModel(Fmu(PATH + ZONE3BIS));
		Simulation Zone4Model(Fmu(PATH + ZONE4));

		IndoorModel.Init(start_time, stop_time);
		Wall1Model.Init(start_time, stop_time);
		Wall2Model.Init(start_time, stop_time);
		Wall3Model.Init(start_time, stop_time);
		Zone1Model.Init(start_time, stop_time);
		Zone2bisModel.Init(start_time, stop_time);
		Zone3bisModel.Init(start_time, stop_time);
		Zone4Model.Init(start_time, stop_time);

		for (int i = 0; i < 10; i++) {
			OutdoorModel.DoStep(0.1);
			IndoorModel.DoStep(0.1);
			Wall1Model.DoStep(0.1);
			Wall2Model.DoStep(0.1);
			Wall3Model.DoStep(0.1);
			Zone1Model.DoStep(0.1);
			Zone2bisModel.DoStep(0.1);
			Zone3bisModel.DoStep(0.1);
			Zone4Model.DoStep(0.1);
		}

		REQUIRE(OutdoorModel.Terminate() == Status::OK);
		REQUIRE(IndoorModel.Terminate() == Status::OK);
		REQUIRE(Wall1Model.Terminate() == Status::OK);
		REQUIRE(Wall2Model.Terminate() == Status::OK);
		REQUIRE(Wall3Model.Terminate() == Status::OK);
		REQUIRE(Zone1Model.Terminate() == Status::OK);
		REQUIRE(Zone2bisModel.Terminate() == Status::OK);
		REQUIRE(Zone3bisModel.Terminate() == Status::OK);
		REQUIRE(Zone4Model.Terminate() == Status::OK);
	}
}
