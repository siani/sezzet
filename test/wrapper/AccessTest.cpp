﻿#include "../catch.h"
#include "../hippomocks.h"
#include "../../src/wrapper/Access.h"

class AccessTest : public Access {
public:
	AccessTest(FmuProxy& proxy, ModelDescription& model_description)
		: Access(proxy, model_description) {}
};

TEST_CASE("AccessTest") {
	MockRepository mocks;
	auto proxy(shared_ptr<FmuProxy>(mocks.Mock<FmuProxy>()));
	auto model_description(shared_ptr<ModelDescription>(mocks.Mock<ModelDescription>()));
	AccessTest access(*proxy, *model_description);

	SECTION("destruction and logging of fmu instances") {
		bool loging_on = false;
		vector<string> categories{ "DEBUG", "INFO" };

		mocks.ExpectCall(proxy.get(), FmuProxy::SetDebugLogging).Return(Status::OK);
		mocks.ExpectCall(proxy.get(), FmuProxy::FreeInstance);

		REQUIRE(access.SetDebugLogging(loging_on, categories) == Status::OK);
		access.FreeInstance();
	}

	SECTION("initialization, termination and resetting an fmu") {
		bool tolerance_defined = false;
		double tolerance = 1e-5;
		double start_time = 0.;
		bool stop_time_defined = true;
		double stop_time = 1.;

		mocks.ExpectCall(proxy.get(), FmuProxy::SetupExperiment)
			.With(tolerance_defined, tolerance, start_time, stop_time_defined, stop_time)
			.Return(Status::OK);
		mocks.ExpectCall(proxy.get(), FmuProxy::EnterInitMode).Return(Status::OK);
		mocks.ExpectCall(proxy.get(), FmuProxy::ExitInitMode).Return(Status::OK);
		mocks.ExpectCall(proxy.get(), FmuProxy::Reset).Return(Status::OK);
		mocks.ExpectCall(proxy.get(), FmuProxy::Terminate).Return(Status::OK);

		REQUIRE(access.SetupExperiment(tolerance_defined, tolerance, start_time, stop_time_defined, stop_time) == Status::OK);
		REQUIRE(access.EnterInitializationMode() == Status::OK);
		REQUIRE(access.ExitInitializationMode() == Status::OK);
		REQUIRE(access.Reset() == Status::OK);
		REQUIRE(access.Terminate() == Status::OK);
	}

	SECTION("getting and setting the complete fmu state") {
		Capabilities capabilities;
		capabilities.Add(Capabilities::CAN_GET_AND_SET_FMU_STATE_);
		capabilities.Add(Capabilities::CAN_SERIALIZE_FMU_STATE_);

		mocks.OnCall(model_description.get(), ModelDescription::Capabilities).Return(std::make_shared<Capabilities>(capabilities));
		mocks.ExpectCall(proxy.get(), FmuProxy::GetFmuState).Return(State::NewEmptyState());
		mocks.ExpectCall(proxy.get(), FmuProxy::SetFmuState).Return(Status::OK);
		mocks.ExpectCall(proxy.get(), FmuProxy::SerializedFmuStateSize).Return(0);
		mocks.ExpectCall(proxy.get(), FmuProxy::SerializeFmuState).Return(vector<char>());
		mocks.ExpectCall(proxy.get(), FmuProxy::DeserializeFmuState).Return(State::NewEmptyState());
		mocks.ExpectCall(proxy.get(), FmuProxy::FreeFmuState).Return(Status::OK);

		auto state = access.GetState(State::NewEmptyState());
		auto set_state_status = access.SetState(state);
		auto size = access.SerializedStateSizeInBytes(state);
		auto state_bytes = access.SerializeState(state, size);
		auto deserialized_state = access.DeserializeState(state_bytes);

		REQUIRE(set_state_status == Status::OK);
		REQUIRE(access.FreeState(state) == Status::OK);
	}

	SECTION("getting partial derivatives") {
		vector<unsigned> unknowns{ 1, 2, 3 };
		vector<unsigned> knowns{ 10, 20, 30 };
		vector<double> unknown_derivatives{ 1., 2., 3. };
		
		mocks.ExpectCall(proxy.get(), FmuProxy::GetDirectionalDerivative).Return(vector < double > {10., 20., 30.});
		
		access.GetDirectionalDerivatives(unknowns, knowns, unknown_derivatives);
	}

	SECTION("transfer of input-output values and parameters") {
		vector<string> variables;
		vector<int> orders;
		vector<double> values;

		mocks.ExpectCall(proxy.get(), FmuProxy::SetRealInputDerivatives).Return(Status::OK);
		mocks.ExpectCall(proxy.get(), FmuProxy::GetRealOutputDerivatives).Return(values);

		Status set_input_derivatives_status = access.SetRealInputDerivatives(variables, orders, values);
		vector<double> output_derivatives = access.GetRealOutputDerivatives(variables, orders);

		REQUIRE(set_input_derivatives_status == Status::OK);
	}

	SECTION("cosimulation computation") {
		double simulation_time = 0.;
		double step_size = 1.;

		mocks.ExpectCall(proxy.get(), FmuProxy::DoStep).Return(Status::OK);
		mocks.ExpectCall(proxy.get(), FmuProxy::CancelStep).Return(Status::OK);

		REQUIRE(access.DoStep(simulation_time, step_size) == Status::OK);
		REQUIRE(access.CancelStep() == Status::OK);
	}

	mocks.ExpectCallDestructor(model_description.get());
	mocks.ExpectCallDestructor(proxy.get());
}