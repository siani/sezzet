﻿#include "../catch.h"
#include "../../src/wrapper/Simulation.h"
#include "../../src/wrapper/Access.h"
#include "../../src/kernel/State.h"

TEST_CASE("indoor fmu state manipulation") {
	SECTION("after calling simulation.GetState the state should not be null") {
		string indoor_path = "C:/cygwin64/home/jcortes/sezzet/test/resources/PackageBuildingCases_Livraison4_01_IndoorHeatGains.fmu";
		Simulation simulation{ Fmu{ indoor_path } };
		simulation.Init(0., 10.);
		
		Access access{ simulation };
		simulation.DoStep(0.1);
		State state_after_one_step = access.GetState(State::NewEmptyState());
		simulation.DoStep(0.1);
		State state_after_two_steps = access.GetState(State::NewEmptyState());
		Status set_state_status = access.SetState(state_after_one_step);
		
		REQUIRE(set_state_status == Status::OK);
		REQUIRE(state_after_one_step.RawPointer() != state_after_two_steps.RawPointer());
		REQUIRE(state_after_one_step.RawPointer() != nullptr);
		REQUIRE(state_after_two_steps.RawPointer() != nullptr);
		
		access.FreeState(state_after_one_step);
		access.FreeState(state_after_two_steps);
		simulation.Terminate();
	}
}