#include "../catch.h"
#include "../../src/modeldescription/v2/ModelDescription.h"
#include "../../src/modeldescription/ModelDescriptionDeserializer.h"
#include "../hippomocks.h"

string xml_path("C:/cygwin64/home/jcortes/sezzet/resources/tank_md.xml");

TEST_CASE("ModelDeserializerTest") {
	SECTION("a deserializer will fill all the class hierarchy") {
		auto model_description(ModelDescriptionDeserializer::AsV2(xml_path));

		REQUIRE(model_description->model_name() == "tankv3");
		REQUIRE(model_description->GetCoSimulation().GetModelIdentifier() == "tankv3");
		REQUIRE(model_description->log_categories().at(0).description() == "logLevel1 - fatal errors");
		REQUIRE(model_description->vendor_annotations().at(0).name() == "JModelica.org");
		REQUIRE(model_description->model_variables().at("_block_jacobian_check")->causality() == "parameter");
		REQUIRE(model_description->model_variables().at("_block_jacobian_check")->initial() == "exact");
		REQUIRE(model_description->model_variables().at("_block_jacobian_check")->type()->name() == "Boolean");
		REQUIRE(model_description->model_variables().at("_block_jacobian_check")->boolean()->start() == false);
	}
}