# What is sezzet? #
Sezzet is a C wrapper to work with the Functional Mock-up Interface [FMI](https://www.fmi-standard.org/).

FMI defines a standardized interface to be used in computer simulations. It was formerly developed by the MODELISAR project and now it is maintained by 
the [Modelica Association Project](https://modelica.org/projects).

The unit of work of the FMI standard is called Functional Mock-up Unit FMU. An FMU is a combination of an xml model description file and a compiled shared library. 

# License #
Sezzet is licensed under the [LGPL](http://www.gnu.org/licenses/lgpl-3.0.txt) v3, the LGPL guarantees that this library will stay open source, protecting your work.

# Team #
* José Juan Hernández-Cabrera (SIANI. University of Las Palmas. SPAIN)
* José Évora-Gómez (SIANI. University of Las Palmas. SPAIN)
* Octavio Roncal-Andrés (SIANI. University of Las Palmas. SPAIN)

# Contributors #
* Enrique Kremers (EIFER. GERMANY)
* Jean-Philippe Tavella (EDF Lab, Paris-Saclay. FRANCE)
* Johan Cortés-Montenegro (SIANI. University of Las Palmas. SPAIN)


**This software has been funded by EIFER - European Institute for Energy Research**