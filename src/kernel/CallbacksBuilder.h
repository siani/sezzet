#pragma once
#include "../fmi/fmi2FunctionTypes.h"
#include <memory>

using std::shared_ptr;

class CallbacksBuilder {
public:
	class CallbacksConfiguration;
	shared_ptr<fmi2CallbackFunctions> DefaultCallbacks();
	CallbacksConfiguration Configure();
};

class CallbacksBuilder::CallbacksConfiguration{
	fmi2CallbackLogger logger;
	fmi2CallbackAllocateMemory allocator;
	fmi2CallbackFreeMemory liberator;
	fmi2StepFinished step_finished;

public:
	CallbacksConfiguration Logger(fmi2CallbackLogger _logger) {
		logger = _logger;
		return *this;
	}
	
	CallbacksConfiguration AllocateMemory(fmi2CallbackAllocateMemory _allocator) {
		allocator = _allocator;
		return *this;
	}
	
	CallbacksConfiguration FreeMemory(fmi2CallbackFreeMemory _liberator) {
		liberator = _liberator;
		return *this;
	}

	CallbacksConfiguration StepFinished(fmi2StepFinished _step_finished) {
		step_finished = _step_finished;
		return *this;
	}
	
	shared_ptr<fmi2CallbackFunctions> Build() {
		return std::make_shared<fmi2CallbackFunctions>(fmi2CallbackFunctions{ logger, allocator, liberator, step_finished, nullptr });
	}
};