#include "WindowsFmiLibrary.h"
#include "../fmi/fmi2FunctionTypes.h"
#include <algorithm>
#include <ostream>
#include <iostream>
#include <string>
#include <sstream>

using std::stringstream;
using std::runtime_error;
using std::transform;
using std::cout;

WindowsFmiLibrary::WindowsFmiLibrary(string library_path) {
	replace(library_path.begin(), library_path.end(), '/', '\\');
	lib_handle = LoadLibrary(library_path.c_str());
	if (lib_handle == nullptr) throw runtime_error(static_cast<std::ostringstream&>(stringstream("Problem loading library ") << library_path << " error code: " << GetLastError()).str());
	instantiate = reinterpret_cast<fmi2InstantiateTYPE*>(GetProcAddress(lib_handle, "fmi2Instantiate"));
	free_instance = reinterpret_cast<fmi2FreeInstanceTYPE*>(GetProcAddress(lib_handle, "fmi2FreeInstance"));
	set_debug_logging = reinterpret_cast<fmi2SetDebugLoggingTYPE*>(GetProcAddress(lib_handle, "fmi2SetDebugLogging"));
	set_up_experiment = reinterpret_cast<fmi2SetupExperimentTYPE*>(GetProcAddress(lib_handle, "fmi2SetupExperiment"));
	enter_init_mode = reinterpret_cast<fmi2EnterInitializationModeTYPE*>(GetProcAddress(lib_handle, "fmi2EnterInitializationMode"));
	exit_init_mode = reinterpret_cast<fmi2ExitInitializationModeTYPE*>(GetProcAddress(lib_handle, "fmi2ExitInitializationMode"));
	terminate = reinterpret_cast<fmi2TerminateTYPE*>(GetProcAddress(lib_handle, "fmi2Terminate"));
	reset = reinterpret_cast<fmi2ResetTYPE*>(GetProcAddress(lib_handle, "fmi2Reset"));
	get_real = reinterpret_cast<fmi2GetRealTYPE*>(GetProcAddress(lib_handle, "fmi2GetReal"));
	get_integer = reinterpret_cast<fmi2GetIntegerTYPE*>(GetProcAddress(lib_handle, "fmi2GetInteger"));
	get_boolean = reinterpret_cast<fmi2GetBooleanTYPE*>(GetProcAddress(lib_handle, "fmi2GetBoolean"));
	get_string = reinterpret_cast<fmi2GetStringTYPE*>(GetProcAddress(lib_handle, "fmi2GetString"));
	set_real = reinterpret_cast<fmi2SetRealTYPE*>(GetProcAddress(lib_handle, "fmi2SetReal"));
	set_integer = reinterpret_cast<fmi2SetIntegerTYPE*>(GetProcAddress(lib_handle, "fmi2SetInteger"));
	set_boolean = reinterpret_cast<fmi2SetBooleanTYPE*>(GetProcAddress(lib_handle, "fmi2SetBoolean"));
	set_string = reinterpret_cast<fmi2SetStringTYPE*>(GetProcAddress(lib_handle, "fmi2SetString"));
	do_step = reinterpret_cast<fmi2DoStepTYPE*>(GetProcAddress(lib_handle, "fmi2DoStep"));
	cancel_step = reinterpret_cast<fmi2CancelStepTYPE*>(GetProcAddress(lib_handle, "fmi2CancelStep"));
	get_fmu_state = reinterpret_cast<fmi2GetFMUstateTYPE*>(GetProcAddress(lib_handle, "fmi2GetFMUstate"));
	free_fmu_state = reinterpret_cast<fmi2FreeFMUstateTYPE*>(GetProcAddress(lib_handle, "fmi2FreeFMUstate"));
	set_fmu_state = reinterpret_cast<fmi2SetFMUstateTYPE*>(GetProcAddress(lib_handle, "fmi2SetFMUstate"));
	serialized_fmu_state_size = reinterpret_cast<fmi2SerializedFMUstateSizeTYPE*>(GetProcAddress(lib_handle, "fmi2SerializedFMUstateSize"));
	serialize_fmu_state = reinterpret_cast<fmi2SerializeFMUstateTYPE*>(GetProcAddress(lib_handle, "fmi2SerializeFMUstate"));
	deserialize_fmu_state = reinterpret_cast<fmi2DeSerializeFMUstateTYPE*>(GetProcAddress(lib_handle, "fmi2DeSerializeFMUstate"));
	set_real_input_derivatives = reinterpret_cast<fmi2SetRealInputDerivativesTYPE*>(GetProcAddress(lib_handle, "fmi2SetRealInputDerivatives"));
	get_real_output_derivatives = reinterpret_cast<fmi2GetRealOutputDerivativesTYPE*>(GetProcAddress(lib_handle, "fmi2GetRealOutputDerivatives"));
}

shared_ptr<void*>  WindowsFmiLibrary::Instantiate(const string& model_name, int fmi_type, const string& guid, const string& resources_location, fmi2CallbackFunctions* callbacks, bool visible, bool logging_on) {
	auto component = std::make_shared<void*>(instantiate(model_name.c_str(), static_cast<fmi2Type>(fmi_type), guid.c_str(), resources_location.c_str(), callbacks, visible, logging_on));
	if (component.get() == nullptr) throw runtime_error("Impossible to instantiate " + model_name + " Instantiate returns null");
	return component;
}

void WindowsFmiLibrary::FreeInstance(void* component) {
	free_instance(component);
	FreeLibrary(lib_handle);
}

int WindowsFmiLibrary::SetDebugLogging(void* component, bool logging_on, size_t number_of_categories, const char* categories[]) {
	return set_debug_logging(component, logging_on, number_of_categories, categories);
}

int WindowsFmiLibrary::SetupExperiment(void* component, bool tolerance_defined, double tolerance, double start_time, bool stop_time_defined, double stop_time) {
	return set_up_experiment(component, tolerance_defined, tolerance, start_time, stop_time_defined, stop_time);
}

int WindowsFmiLibrary::EnterInitializationMode(void* component) {
	return enter_init_mode(component);
}

int WindowsFmiLibrary::ExitInitializationMode(void* component) {
	return exit_init_mode(component);
}

int WindowsFmiLibrary::Terminate(void* component) {
	return terminate(component);
}

int WindowsFmiLibrary::Reset(void* component) {
	return reset(component);
}

int WindowsFmiLibrary::SetReal(void* component, const unsigned value_references[], size_t number_of_value_references, const double values[]) {
	return set_real(component, value_references, number_of_value_references, values);
}

int WindowsFmiLibrary::SetInteger(void* component, const unsigned value_references[], size_t number_of_value_references, const int values[]) {
	return set_integer(component, value_references, number_of_value_references, values);
}

int WindowsFmiLibrary::SetBoolean(void* component, const unsigned value_references[], size_t number_of_value_references, const int values[]) {
	return set_boolean(component, value_references, number_of_value_references, values);
}

int WindowsFmiLibrary::SetString(void* component, const unsigned value_references[], size_t number_of_value_references, const char* values[]) {
	return set_string(component, value_references, number_of_value_references, values);
}

int WindowsFmiLibrary::GetReal(void* component, const unsigned value_references[], size_t number_of_value_references, double output[]) {
	return get_real(component, value_references, number_of_value_references, output);
}

int WindowsFmiLibrary::GetInteger(void* component, const unsigned value_references[], size_t number_of_value_references, int output[]) {
	return get_integer(component, value_references, number_of_value_references, output);
}

int WindowsFmiLibrary::GetBoolean(void* component, const unsigned value_references[], size_t number_of_value_references, int output[]) {
	return get_boolean(component, value_references, number_of_value_references, output);
}

int WindowsFmiLibrary::GetString(void* component, const unsigned value_references[], size_t number_of_value_references, char* output[]) {
	return get_string(component, value_references, number_of_value_references, const_cast<char const**>(output));
}

int WindowsFmiLibrary::DoStep(void* component, double communication_point, double step_size) {
	return do_step(component, communication_point, step_size, false);
}

int WindowsFmiLibrary::CancelStep(void* component) {
	return cancel_step(component);
}

int WindowsFmiLibrary::GetFmuState(void* component, void** pointer_to_state) {
	return get_fmu_state(component, pointer_to_state);
}

int WindowsFmiLibrary::FreeFmuState(void* component, void** pointer_to_state) {
	return free_fmu_state(component, pointer_to_state);
}

int WindowsFmiLibrary::SetFmuState(void* component, void* state) {
	return set_fmu_state(component, state);
}

int WindowsFmiLibrary::SerializedFMUstateSize(void* component, fmi2FMUstate state, size_t* size) {
	return serialized_fmu_state_size(component, state, size);
}

int WindowsFmiLibrary::SerializeState(void* component, fmi2FMUstate state, fmi2Byte data[], size_t size) {
	return serialize_fmu_state(component, state, data, size);
}

int WindowsFmiLibrary::DesrializeState(void* component, const fmi2Byte state_bytes[], size_t size, fmi2FMUstate* deserialized_state) {
	return deserialize_fmu_state(component, state_bytes, size, deserialized_state);
}

int WindowsFmiLibrary::GetDirectionalDerivative(void* component, const unsigned unknown_value_references[], size_t number_of_unknowns, const unsigned known_value_references[], size_t number_of_knowns, const double known_derivatives[], double unknown_derivatives[]) {
	return get_directional_derivative(component, unknown_value_references, number_of_unknowns, known_value_references, number_of_knowns, known_derivatives, unknown_derivatives);
}

int WindowsFmiLibrary::SetRealInputDerivatives(void* component, const unsigned value_references[], size_t number_of_value_references, const int orders[], const double values[]) {
	return set_real_input_derivatives(component, value_references, number_of_value_references, orders, values);
}

int WindowsFmiLibrary::GetRealOutputDerivatives(void* component, const unsigned value_references[], size_t number_of_value_references, const int orders[], double output[]) {
	return 	get_real_output_derivatives(component, value_references, number_of_value_references, orders, output);
}

