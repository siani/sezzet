#pragma once

#include "../FmiLibrary.h"
#include "../Status.h"
#include "../State.h"
#include <memory>

using std::unique_ptr;

class FmuProxy {
	shared_ptr<fmi2CallbackFunctions> callbacks;
	shared_ptr<FmiLibrary> library;
	shared_ptr<void*> component;

public:
	FmuProxy(shared_ptr<FmiLibrary> library);
	virtual ~FmuProxy();
	virtual void Instantiate(const string& model_identifier, const string& guid);
	virtual Status SetDebugLogging(bool enable_loggin, const vector<string>& categories);
	virtual void FreeInstance();

	virtual Status SetupExperiment(bool toleranceDefined, double tolerance, double start_time, bool stop_time_defined, double stop_time);
	virtual Status EnterInitMode();
	virtual Status ExitInitMode();
	virtual Status Terminate();
	virtual Status Reset();

	virtual vector<double> GetReal(const vector<unsigned>& value_references);
	virtual vector<int> GetInteger(const vector<unsigned>& value_references);
	virtual vector<bool> GetBoolean(const vector<unsigned>& value_references);
	virtual vector<string> GetString(const vector<unsigned>& value_references);
	virtual Status SetReal(const vector<unsigned>& value_references, const vector<double>& reals);
	virtual Status SetInteger(const vector<unsigned>& value_references, const vector<int>& integers);
	virtual Status SetBoolean(const vector<unsigned>& value_references, const vector<bool>& booleans);
	virtual Status SetString(const vector<unsigned>& value_references, const vector<string>& strings);

	virtual Status DoStep(double communication_point, double step_size);
	virtual Status CancelStep();

	virtual State GetFmuState(State state);
	virtual Status FreeFmuState(State state);
	virtual Status SetFmuState(State state);
	virtual unsigned long long SerializedFmuStateSize(State state);
	virtual vector<char> SerializeFmuState(State state, unsigned long long size_int_bytes);
	virtual State DeserializeFmuState(const vector<char>& state_bytes);

	virtual vector<double> GetDirectionalDerivative(const vector<unsigned>& unknowns_value_references, const vector<unsigned>& knowns_value_references, const vector<double>& known_derivatives);

	virtual Status SetRealInputDerivatives(const vector<unsigned>& value_references, const vector<int>& orders, const vector<double>& values);
	virtual vector<double> GetRealOutputDerivatives(const vector<unsigned>& value_references, const vector<int>& orders);
};
