#include "FmuProxy.h"
#include "../CallbacksBuilder.h"
#include <iterator>
#include <algorithm>

FmuProxy::FmuProxy(shared_ptr<FmiLibrary> library) : 
callbacks(CallbacksBuilder().DefaultCallbacks()),
library(library) {}

FmuProxy::~FmuProxy() {}

void FmuProxy::Instantiate(const std::basic_string<char>& model_identifier, const std::basic_string<char>& guid) {
	component = library->Instantiate(model_identifier, FmiLibrary::CO_SIMULATION, guid, "file://", callbacks.get(), false, false);
}

Status FmuProxy::SetDebugLogging(bool enable_loggin, const vector<string>& categories) {
	vector<const char*> categories_as_native;
	transform(categories.begin(), 
		categories.end(), 
		std::back_inserter(categories_as_native),
		[](const string& category) {return category.c_str(); });
	return Status(library->SetDebugLogging(*component, enable_loggin, categories.size(), categories_as_native.data()));
}

void FmuProxy::FreeInstance() {
	library->FreeInstance(*component);
}

Status FmuProxy::SetupExperiment(bool toleranceDefined, double tolerance, double start_time, bool stop_time_defined, double stop_time) {
	return Status(library->SetupExperiment(*component, toleranceDefined, tolerance, start_time, stop_time_defined, start_time));
}

Status FmuProxy::EnterInitMode() {
	return Status(library->EnterInitializationMode(*component));
}

Status FmuProxy::ExitInitMode() {
	return Status(library->ExitInitializationMode(*component));
}

Status FmuProxy::Terminate() {
	return Status(library->Terminate(*component));
}

Status FmuProxy::Reset() {
	return Status(library->Reset(*component));
}

vector<double> FmuProxy::GetReal(const vector<unsigned>& value_references) {
	vector<double> values(value_references.size());
	library->GetReal(*component, &value_references.at(0), value_references.size(), &values.at(0));
	return values;
}

vector<int> FmuProxy::GetInteger(const vector<unsigned>& value_references) {
	vector<int> values(value_references.size());
	library->GetInteger(*component, &value_references.at(0), value_references.size(), &values.at(0));
	return values;
}

vector<bool> FmuProxy::GetBoolean(const vector<unsigned>& value_references) {
	vector<int> values_as_int(value_references.size());
	library->GetBoolean(*component, &value_references.at(0), value_references.size(), &values_as_int[0]);
	vector<bool> values;
	transform(values_as_int.begin(), values_as_int.end(), std::back_inserter(values), [](const int& value) -> bool { return value == 1; });
	return values;
}

vector<std::basic_string<char>> FmuProxy::GetString(const vector<unsigned>& value_references) {
	vector<char*> values_as_native(value_references.size());
	library->GetString(*component, &value_references.at(0), value_references.size(), values_as_native.data());
	vector<string> values;
	transform(values_as_native.begin(), values_as_native.end(), std::back_inserter(values), [](const char* _value) {return string(_value); });
	return values;
}

Status FmuProxy::SetReal(const vector<unsigned>& value_references, const vector<double>& reals) {
	return Status(library->SetReal(*component, &value_references.at(0), value_references.size(), &reals.at(0)));
}

Status FmuProxy::SetInteger(const vector<unsigned>& value_references, const vector<int>& integers) {
	return Status(library->SetInteger(*component, &value_references.at(0), value_references.size(), &integers.at(0)));
}

Status FmuProxy::SetBoolean(const vector<unsigned>& value_references, const vector<bool>& booleans) {
	vector<int> booleans_as_int;
	transform(booleans.begin(), booleans.end(), back_inserter(booleans_as_int), [](const bool& value) { return value ? 1 : 0; });
	return Status(library->SetBoolean(*component, &value_references.at(0), value_references.size(), &booleans_as_int.at(0)));
}

Status FmuProxy::SetString(const vector<unsigned>& value_references, const vector<string>& strings) {
	vector<const char*> strings_as_native;
	transform(strings.begin(), strings.end(), std::back_inserter(strings_as_native), [](const string& _value) { return const_cast<char*>(_value.c_str()); });
	strings_as_native.push_back(nullptr);
	return Status(library->SetString(*component, &value_references.at(0), value_references.size(), strings_as_native.data()));
}

Status FmuProxy::DoStep(double communication_point, double step_size) {
	return Status(library->DoStep(*component, communication_point, step_size));
}

Status FmuProxy::CancelStep() {
	return Status(library->CancelStep(*component));
}

State FmuProxy::GetFmuState(State state) {
	auto raw_state = state.RawPointer();
	if (Status(library->GetFmuState(*component, &raw_state)) != Status::OK)
		throw std::runtime_error("failed call to fmi2GetFMUstatus");
	state.RawPointer(raw_state);
	return state;
}

Status FmuProxy::FreeFmuState(State state) {
	auto raw_state = state.RawPointer();
	return Status(library->FreeFmuState(*component, &raw_state));
}

Status FmuProxy::SetFmuState(State state) {
	return Status(library->SetFmuState(*component, state.RawPointer()));
}

unsigned long long FmuProxy::SerializedFmuStateSize(State state) {
	size_t size = 0;
	library->SerializedFMUstateSize(*component, state.RawPointer(), &size);
	return size;
}

vector<char> FmuProxy::SerializeFmuState(State state, unsigned long long size_int_bytes) {
	vector<char> data(size_int_bytes);
	library->SerializeState(*component, state.RawPointer(), &data.at(0), size_int_bytes);
	return data;
}

State FmuProxy::DeserializeFmuState(const vector<char>& state_bytes) {
	auto deserialized_state(State::NewEmptyState());
	auto raw_pointer = deserialized_state.RawPointer();
	library->DesrializeState(*component, &state_bytes.at(0), state_bytes.size(), &raw_pointer);
	return deserialized_state;
}

vector<double> FmuProxy::GetDirectionalDerivative(const vector<unsigned>& unknowns_value_references, const vector<unsigned>& knowns_value_references, const vector<double>& known_derivatives) {
	vector<double> unknown_derivatives(unknowns_value_references.size());
	library->GetDirectionalDerivative(*component, &unknowns_value_references.at(0), unknowns_value_references.size(), &knowns_value_references.at(0), knowns_value_references.size(), &known_derivatives.at(0), &unknown_derivatives.at(0));
	return unknown_derivatives;
}

Status FmuProxy::SetRealInputDerivatives(const vector<unsigned>& value_references, const vector<int>& orders, const vector<double>& values) {
	return Status(library->SetRealInputDerivatives(*component, &value_references.at(0), value_references.size(), &orders.at(0), &values.at(0)));
}

vector<double> FmuProxy::GetRealOutputDerivatives(const vector<unsigned>& value_references, const vector<int>& orders) {
	vector<double> outputs(value_references.size());
	library->GetRealOutputDerivatives(*component, &value_references.at(0), value_references.size(), &orders.at(0), &outputs[0]);
	return outputs;
}