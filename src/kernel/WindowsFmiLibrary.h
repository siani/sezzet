#pragma once
#include "FmiLibrary.h"
#include <Windows.h>

class WindowsFmiLibrary : public FmiLibrary {
	HMODULE lib_handle;

public:
	WindowsFmiLibrary(string library_path);

	shared_ptr<void*> Instantiate(const string& model_name, int fmi_type, const string& guid, const string& resources_location, fmi2CallbackFunctions* callbacks, bool visible, bool logging_on) override;
	void FreeInstance(void* component) override;
	int SetDebugLogging(void* component, bool logging_on, size_t number_of_categories, const char* categories[]) override;
	int SetupExperiment(void* component, bool tolerance_defined, double tolerance, double start_time, bool stop_time_defined, double stop_time) override;

	int EnterInitializationMode(void* component) override;
	int ExitInitializationMode(void* component) override;
	int Terminate(void* component) override;
	int Reset(void* component) override;

	int SetReal(void* component, const unsigned value_references[], size_t number_of_value_references, const double values[]) override;
	int SetInteger(void* component, const unsigned value_references[], size_t number_of_value_references, const int values[]) override;
	int SetBoolean(void* component, const unsigned value_references[], size_t number_of_value_references, const int values[]) override;
	int SetString(void* component, const unsigned value_references[], size_t number_of_value_references, const char* values[]) override;
	int GetReal(void* component, const unsigned value_references[], size_t number_of_value_references, double output[]) override;
	int GetInteger(void* component, const unsigned value_references[], size_t number_of_value_references, int output[]) override;
	int GetBoolean(void* component, const unsigned value_references[], size_t number_of_value_references, int output[]) override;
	int GetString(void* component, const unsigned value_references[], size_t number_of_value_references, char* output[]) override;
	int DoStep(void* component, double communication_point, double step_size) override;
	int CancelStep(void* component) override;

	int GetFmuState(void*, void** pointer_to_state) override;
	int FreeFmuState(void* component, void** pointer_to_state) override;
	int SetFmuState(void* component, void* state) override;
	int SerializedFMUstateSize(void* component, void* state, size_t* size) override;
	int SerializeState(void* component, void* state, char data[], size_t size) override;
	int DesrializeState(void* component, const char state_bytes[], size_t size, void** deserialized_state) override;

	int GetDirectionalDerivative(void* component, const unsigned unknown_value_references[], size_t number_of_unknowns, const unsigned known_value_references[], size_t number_of_knowns, const double known_derivatives[], double unknown_derivatives[]) override;

	int SetRealInputDerivatives(void* component, const unsigned value_references[], size_t number_of_value_references, const int orders[], const double values[]) override;
	int GetRealOutputDerivatives(void* component, const unsigned value_references[], size_t number_of_value_references, const int orders[], double output[]) override;
};
