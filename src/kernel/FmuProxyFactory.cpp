#include "FmuProxyFactory.h"
#include "WindowsFmiLibrary.h"

unique_ptr<FmuProxy> FmuProxyFactory::Create(string library_path) {
	shared_ptr<FmiLibrary> library(new WindowsFmiLibrary(library_path));
	return std::make_unique<FmuProxy>(library);
}