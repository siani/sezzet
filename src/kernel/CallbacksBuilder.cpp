#include <string>
#include <iostream>
#include <cstdlib>
#include "CallbacksBuilder.h"
using std::string;
using std::cout;

string getCategory(string category) {
	return category.empty() ? "" : ":" + category;
}

void logMessage(fmi2ComponentEnvironment environment, fmi2String _instance_name, fmi2Status status, fmi2String category, fmi2String message, ...) {
	string instance_name(_instance_name);
	cout << "[" + instance_name << getCategory(category) << "] " << message << std::endl;
}

shared_ptr<fmi2CallbackFunctions> CallbacksBuilder::DefaultCallbacks() {
	return CallbacksBuilder().Configure()
		.Logger(logMessage)
		.AllocateMemory(std::calloc)
		.FreeMemory(std::free)
		.StepFinished(nullptr)
		.Build();
}

CallbacksBuilder::CallbacksConfiguration CallbacksBuilder::Configure() {
	return CallbacksConfiguration();
}