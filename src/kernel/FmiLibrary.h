#pragma once
#include <vector>
#include "../fmi/fmi2FunctionTypes.h"
#include <memory>

enum class Status;
using std::string;
using std::vector;
using std::shared_ptr;
class FmiLibrary {
protected:
	fmi2InstantiateTYPE* instantiate;
	fmi2FreeInstanceTYPE* free_instance;
	fmi2SetDebugLoggingTYPE* set_debug_logging;
	fmi2SetupExperimentTYPE* set_up_experiment;
	fmi2EnterInitializationModeTYPE* enter_init_mode;
	fmi2ExitInitializationModeTYPE* exit_init_mode;
	fmi2TerminateTYPE* terminate;
	fmi2ResetTYPE* reset;
	fmi2GetRealTYPE* get_real;
	fmi2GetIntegerTYPE* get_integer;
	fmi2GetBooleanTYPE* get_boolean;
	fmi2GetStringTYPE* get_string;
	fmi2SetRealTYPE* set_real;
	fmi2SetIntegerTYPE* set_integer;
	fmi2SetBooleanTYPE* set_boolean;
	fmi2SetStringTYPE* set_string;
	fmi2DoStepTYPE* do_step;
	fmi2CancelStepTYPE* cancel_step;
	fmi2GetFMUstateTYPE* get_fmu_state;
	fmi2FreeFMUstateTYPE* free_fmu_state;
	fmi2SetFMUstateTYPE* set_fmu_state;
	fmi2SerializedFMUstateSizeTYPE* serialized_fmu_state_size;
	fmi2SerializeFMUstateTYPE* serialize_fmu_state;
	fmi2DeSerializeFMUstateTYPE* deserialize_fmu_state;
	fmi2SetRealInputDerivativesTYPE* set_real_input_derivatives;
	fmi2GetRealOutputDerivativesTYPE* get_real_output_derivatives;
	fmi2GetDirectionalDerivativeTYPE* get_directional_derivative;

public:
	static const int CO_SIMULATION = 1;

	virtual ~FmiLibrary() {}

	virtual shared_ptr<void*> Instantiate(const string& model_name, int fmi_type, const string& guid, const string& resources_location, fmi2CallbackFunctions* callbacks, bool visible, bool logging_on) = 0;
	virtual void FreeInstance(void* component) = 0;
	virtual int SetDebugLogging(void* component, bool logging_on, size_t number_of_categories, const char* categories[]) = 0;

	virtual int SetupExperiment(void* component, bool tolerance_defined, double tolerance, double start_time, bool stop_time_defined, double stop_time) = 0;
	virtual int EnterInitializationMode(void* component) = 0;
	virtual int ExitInitializationMode(void* component) = 0;
	virtual int Terminate(void* component) = 0;
	virtual int Reset(void* component) = 0;

	virtual int SetReal(void* component, const unsigned value_references[], size_t number_of_value_references, const double values[]) = 0;
	virtual int SetInteger(void* component, const unsigned value_references[], size_t number_of_value_references, const int values[]) = 0;
	virtual int SetBoolean(void* component, const unsigned value_references[], size_t number_of_value_references, const int values[]) = 0;
	virtual int SetString(void* component, const unsigned value_references[], size_t number_of_value_references, const char* values[]) = 0;

	virtual int GetReal(void* component, const unsigned value_references[], size_t number_of_value_references, double output[]) = 0;
	virtual int GetInteger(void* component, const unsigned value_references[], size_t number_of_value_references, int output[]) = 0;
	virtual int GetBoolean(void* component, const unsigned value_references[], size_t number_of_value_references, int output[]) = 0;
	virtual int GetString(void* component, const unsigned value_references[], size_t number_of_value_references, char* output[]) = 0;

	virtual int DoStep(void* component, double communication_point, double step_size) = 0;
	virtual int CancelStep(void* component) = 0;

	virtual int GetFmuState(void*, void** pointer_to_state) = 0;
	virtual int FreeFmuState(void* component, void** pointer_to_state) = 0;
	virtual int SetFmuState(void* component, void* state) = 0;
	virtual int SerializedFMUstateSize(void* component, void* state, size_t* size) = 0;
	virtual int SerializeState(void* component, void* state, char data[], size_t size) = 0;
	virtual int DesrializeState(void* component, const char state_bytes[], size_t size, void** deserialized_state) = 0;

	virtual int GetDirectionalDerivative(void* component, const unsigned unknown_value_references[], size_t number_of_unknowns, const unsigned known_value_references[], size_t number_of_knowns, const double known_derivatives[], double unknown_derivatives[]) = 0;

	virtual int SetRealInputDerivatives(void* component, const unsigned value_references[], size_t number_of_value_references, const int orders[], const double values[]) = 0;
	virtual int GetRealOutputDerivatives(void* component, const unsigned value_references[], size_t number_of_value_references, const int orders[], double output[]) = 0;
};
