﻿#include "State.h"

State::State(void* raw_state): raw_state(raw_state) {}

void* State::RawPointer() {
	return raw_state;
}

void State::RawPointer(void* raw_state) {
	this->raw_state = raw_state;
}

State State::NewEmptyState() {
	return State(nullptr);
}
