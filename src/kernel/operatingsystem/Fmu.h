#pragma once

#include <memory>
#include <stdexcept>
#include "SevenZipUnzipper.h"
#include "../../modeldescription/v2/ModelDescription.h"

using std::runtime_error;
using std::shared_ptr;

class Fmu {
	string fmu_file_path;
	string working_directory_path;
	string model_description_path;
	string library_path;

public:
	Fmu(string fmu_file_path);
	virtual ~Fmu();
	virtual string WorkingDirectoryPath();
	virtual string ModelDescriptionPath();
	virtual shared_ptr<ModelDescription> ModelDescription();
	virtual string LibraryPath(string machine_info, string model_name);
private:
	string MakeTemporaryDirectory();
	string BuildTemporaryDirectoryPath();
};
