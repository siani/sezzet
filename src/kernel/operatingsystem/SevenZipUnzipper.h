#include <string>
#pragma once
using std::string;

class SevenZipUnzipper {
	string file_path;
public:
	SevenZipUnzipper(string file_path);
	~SevenZipUnzipper();
	void ExtractTo(string output_path);
};