#include "Fmu.h"
#include <memory>
#include <chrono>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include "../../modeldescription/ModelDescriptionDeserializer.h"


using boost::filesystem::temp_directory_path;
using boost::filesystem::path;
using boost::filesystem::create_directory;
using boost::lexical_cast;
using std::chrono::milliseconds;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::make_shared;

Fmu::Fmu(string fmu_file_path) :
	fmu_file_path(fmu_file_path),
	working_directory_path(MakeTemporaryDirectory()),
	model_description_path(working_directory_path + "/modelDescription.xml") {
	SevenZipUnzipper(fmu_file_path).ExtractTo(working_directory_path);
}

Fmu::~Fmu() {}

string Fmu::MakeTemporaryDirectory() {
	string temp_folder = BuildTemporaryDirectoryPath();
	create_directory(path(temp_folder));
	return temp_folder;
}

string Fmu::BuildTemporaryDirectoryPath() {
	path fmu_path{ fmu_file_path };
	string temp_folder_path = temp_directory_path().generic_string() +
		"fmu_" +
		fmu_path.filename().string();
	return temp_folder_path;
}

string Fmu::LibraryPath(string machine_info, string model_name) {
	if (library_path.empty())	library_path = working_directory_path + "/binaries/" + machine_info + "/" + model_name + ".dll";
	return library_path;
}

string Fmu::WorkingDirectoryPath() {
	return working_directory_path;
}

string Fmu::ModelDescriptionPath() {
	return model_description_path;
}

shared_ptr<ModelDescription> Fmu::ModelDescription() {
	return ModelDescriptionDeserializer::AsV2(ModelDescriptionPath());
}