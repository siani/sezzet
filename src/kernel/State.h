﻿#pragma once

class State {
	void* raw_state;

public:
	void* RawPointer();
	void RawPointer(void* raw_state);
	static State NewEmptyState();

private:
	State(void* raw_state);
};
