#pragma once
#include "v2/FmuProxy.h"
#include "operatingsystem/Fmu.h"

namespace FmuProxyFactory {
	unique_ptr<FmuProxy> Create(string libraryPath);
};

