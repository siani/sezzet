﻿#include "Access.h"

Access::Access(Simulation& simulation)
	: Access(ProxyRetriever().GetProxy(simulation), simulation.GetModelDescription()) {}

Access::Access(FmuProxy& proxy, ModelDescription& model_description) :
proxy_(proxy),
model_description_(model_description) {}

Access::~Access() {}

Status Access::SetDebugLogging(bool loging_on, vector<string>& categories) {
	return proxy_.SetDebugLogging(loging_on, categories);
}

void Access::FreeInstance() {
	proxy_.FreeInstance();
}

Status Access::SetupExperiment(bool tolerance_defined, double tolerance, double start_time, bool stop_time_defined, double stop_time) {
	return proxy_.SetupExperiment(tolerance_defined, tolerance, start_time, stop_time_defined, stop_time);
}

Status Access::EnterInitializationMode() {
	return proxy_.EnterInitMode();
}

Status Access::ExitInitializationMode() {
	return proxy_.ExitInitMode();
}

Status Access::Reset() {
	return proxy_.Reset();
}

Status Access::Terminate() {
	return proxy_.Terminate();
}

State Access::GetState(State state) {
	CheckCanGetAndSetFmuState();
	return proxy_.GetFmuState(state);
}

Status Access::FreeState(State state) {
	CheckCanGetAndSetFmuState();
	return proxy_.FreeFmuState(state);
}

Status Access::SetState(State state) {
	CheckCanGetAndSetFmuState();
	return proxy_.SetFmuState(state);
}

unsigned long long Access::SerializedStateSizeInBytes(State state) {
	CheckCanGetAndSetFmuState();
	CheckCanSerializeFmuState();
	return proxy_.SerializedFmuStateSize(state);
}

vector<char> Access::SerializeState(State state, unsigned long long size_in_bytes) {
	CheckCanGetAndSetFmuState();
	CheckCanSerializeFmuState();
	return proxy_.SerializeFmuState(state, size_in_bytes);
}

State Access::DeserializeState(vector<char>& state_bytes) {
	CheckCanGetAndSetFmuState();
	CheckCanSerializeFmuState();
	return proxy_.DeserializeFmuState(state_bytes);
}

vector<double> Access::GetDirectionalDerivatives(const vector<unsigned>& unknown_value_references, const vector<unsigned>& knowns_value_references, const vector<double>& unknown_derivatives) {
	return proxy_.GetDirectionalDerivative(unknown_value_references, knowns_value_references, unknown_derivatives);
}

void Access::CheckCanGetAndSetFmuState() {
	if (model_description_.Capabilities()->Check(Capabilities::CAN_GET_AND_SET_FMU_STATE_))
		return;
	throw runtime_error("this FMU does not provide support for get and set state operations");
}

void Access::CheckCanSerializeFmuState() {
	if (model_description_.Capabilities()->Check(Capabilities::CAN_SERIALIZE_FMU_STATE_))
		return;
	throw runtime_error("this FMU does not provide support for state serialization operations");
}

Status Access::SetRealInputDerivatives(vector<string>& variables, vector<int>& orders, vector<double>& values) {	
	return proxy_.SetRealInputDerivatives(model_description_.GetValueReferences(variables), orders, values);
}

vector<double> Access::GetRealOutputDerivatives(vector<string>& variables, vector<int>& orders) {
	return proxy_.GetRealOutputDerivatives(model_description_.GetValueReferences(variables), orders);
}

Status Access::DoStep(double simulation_time, double step_size) {
	return proxy_.DoStep(simulation_time, step_size);
}

Status Access::CancelStep() {
	return proxy_.CancelStep();
}
