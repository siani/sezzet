#include <memory>
#include <utility>
#include "Simulation.h"
#include "../kernel/FmuProxyFactory.h"
#include <iostream>
#include <boost/filesystem/operations.hpp>

using std::make_shared;

Simulation::Simulation(Fmu&& fmu) {
	working_directory_path_ = fmu.WorkingDirectoryPath();
	model_description_ = fmu.ModelDescription();
	proxy_ = FmuProxyFactory::Create(fmu.LibraryPath("win64", model_description_->GetModelIdentifier()));
	proxy_->Instantiate(model_description_->GetModelIdentifier(), model_description_->GetGuID());
}

Simulation::~Simulation() {
	boost::filesystem::remove_all(boost::filesystem::path(working_directory_path_));
}

ModelDescription& Simulation::GetModelDescription() {
	return *model_description_;
}

Status Simulation::Init(double start_time, double stop_time) {
	simulation_time_ = start_time;
	SetUpInitialValues();
	if (proxy_->SetupExperiment(true, 1e-6, start_time, true, stop_time) != Status::OK) throw std::runtime_error("problem on call to SetupExperiment");
	if (proxy_->EnterInitMode() != Status::OK) throw std::runtime_error("problem on call to EnterInitializationMode");
	if (proxy_->ExitInitMode() != Status::OK) throw std::runtime_error("problem on call to ExitInitializationMode");
	return Status::OK;
}

void Simulation::SetUpInitialValues() {
	for (auto variable_pair : model_description_->model_variables()) {
		auto variable = variable_pair.second;
		if (variable->initial() == "exact") {
			if (variable->type()->name() == "Real") Write(variable->name()).With(variable->real()->start());
			else if (variable->type()->name() == "Integer") Write(variable->name()).With(variable->integer()->start());
			else if (variable->type()->name() == "Boolean") Write(variable->name()).With(variable->boolean()->start());
			else if (variable->type()->name() == "String") Write(variable->name()).With(variable->stringg()->start());
		}
	}
}

Status Simulation::Terminate() {
	auto terminate_status = proxy_->Terminate();
	if (terminate_status == Status::OK) proxy_->FreeInstance();
	return terminate_status;
}

Status Simulation::Reset() {
	return proxy_->Reset();
}

Status Simulation::DoStep(double step_size) {
	auto step_status = proxy_->DoStep(simulation_time_, step_size);
	if (step_status == Status::OK) simulation_time_ += step_size;
	return step_status;
}

WriteCall Simulation::Write(string variable_name) { return Write(vector < string > { variable_name }); }


WriteCall Simulation::Write(vector<string> variable_names) {
	return WriteCall(*proxy_, model_description_->GetValueReferences(variable_names));
}

ReadCall Simulation::Read(string variable_name) { return Read(vector < string > {variable_name}); }

ReadCall Simulation::Read(vector<string> variable_names) {
	return ReadCall(*proxy_, model_description_->GetValueReferences(variable_names));
}

FmuProxy& Simulation::GetProxy(Simulation& simulation) {
	return *simulation.proxy_;
}