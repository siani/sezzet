#pragma once
#include "../kernel/Status.h"
#include "../kernel/operatingsystem/Fmu.h"
#include "../modeldescription/v2/ModelDescription.h"
#include "../kernel/v2/FmuProxy.h"
#include <memory>

using std::unique_ptr;
class Simulation;

class WriteCall {

	FmuProxy& proxy;
	vector<unsigned> value_references;

public:
	WriteCall(FmuProxy& proxy, vector<unsigned>&& value_references) : proxy(proxy), value_references(value_references) {}
	Status With(double value) { return  With(vector < double > { value }); }
	Status With(int value) { return  With(vector < int > { value }); }
	Status With(bool value) { return  With(vector < bool > { value }); }
	Status With(string value) { return  With(vector < string > { value }); }

	Status With(const vector<double>& values) {
		return proxy.SetReal(value_references, values);
	}
	Status With(const vector<int>& values) {
		return proxy.SetInteger(value_references, values);
	}
	Status With(const vector<bool>& values) {
		return proxy.SetBoolean(value_references, values);
	}
	Status With(const vector<string>& values) {
		return proxy.SetString(value_references, values);
	}
};

class ReadCall {
	FmuProxy& proxy;
	vector<unsigned> value_references;

public:
	ReadCall(FmuProxy& proxy, vector<unsigned>&& value_references) : proxy(proxy), value_references(value_references) {}
	vector<double> AsDouble() { return proxy.GetReal(value_references); }
	vector<int> AsInteger() { return proxy.GetInteger(value_references); }
	vector<bool> AsBoolean() { return proxy.GetBoolean(value_references); }
	vector<string> AsString() { return proxy.GetString(value_references); }
};

class Simulation {

	shared_ptr<ModelDescription> model_description_;
	double simulation_time_;
	string working_directory_path_;
	shared_ptr<FmuProxy> proxy_;

public:
	Simulation(Fmu&& fmu);
	virtual ~Simulation();
	ModelDescription& GetModelDescription();

	virtual Status Init(double start_time, double stop_time);
	virtual Status Terminate();

	virtual Status Reset();
	WriteCall Write(string variable_name);
	WriteCall Write(vector<string> variable_names);
	Status DoStep(double step_size);
	ReadCall Read(string variable_name);
	ReadCall Read(vector<string> variable_names);

protected:
	Simulation() {}
	static FmuProxy& GetProxy(Simulation& simulation);
private:
	void SetUpInitialValues();
};

