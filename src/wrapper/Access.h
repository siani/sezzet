﻿#pragma once
#include "Simulation.h"
#include "../kernel/State.h"

class Access{

	FmuProxy& proxy_;
	ModelDescription& model_description_;

public:
	Access(Simulation& simulation);
	virtual ~Access();

	Status SetDebugLogging(bool loging_on, vector<string>& categories);
	void FreeInstance();

	Status SetupExperiment(bool tolerance_defined, double tolerance, double start_time, bool stop_time_defined, double stop_time);
	Status EnterInitializationMode();
	Status ExitInitializationMode();
	Status Reset();
	Status Terminate();

	State GetState(State state);
	Status FreeState(State state);
	Status SetState(State state);
	unsigned long long SerializedStateSizeInBytes(State state);
	vector<char> SerializeState(State state, unsigned long long size_in_bytes);
	State DeserializeState(vector<char>& state_bytes);
	
	vector<double> GetDirectionalDerivatives(const vector<unsigned>& unknown_value_references, const vector<unsigned>& knowns_value_references, const vector<double>& unknown_derivatives);

	Status SetRealInputDerivatives(vector<string>& variables, vector<int>& orders, vector<double>& values);
	vector<double> GetRealOutputDerivatives(vector<string>& variables, vector<int>& orders);
	Status DoStep(double simulation_time, double step_size);
	Status CancelStep();

protected:
	Access(FmuProxy& proxy, ModelDescription& model_description);

private:
	void CheckCanGetAndSetFmuState();
	void CheckCanSerializeFmuState();
};

class ProxyRetriever :public Simulation {
public:
	FmuProxy& GetProxy(Simulation& simulation) {
		return Simulation::GetProxy(simulation);
	}
};
