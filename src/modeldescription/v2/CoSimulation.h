#ifndef MODELDESCRIPTION_V2_COSIMULATION_H_
#define MODELDESCRIPTION_V2_COSIMULATION_H_

#include <vector>
#include "SourceFile.h"
#include "Capabilities.h"
#include <memory>

using std::vector;
using std::shared_ptr;

class CoSimulation{
private:
	string model_identifier_;
	int max_output_derivative_order_;

	vector<SourceFile> source_files_;
	shared_ptr<Capabilities> capabilities_;
public:
	CoSimulation();
	virtual ~CoSimulation();
	void max_output_derivative_order(int new_max_output_derivative_order);
	int max_output_derivative_order();
	void model_identifier(string new_model_identifier);
	virtual string GetModelIdentifier();    
	shared_ptr<Capabilities> GetCapabilities();
	void AddSourceFile(SourceFile source_file);
	vector<SourceFile> source_files();
};


#endif
