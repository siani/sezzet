#include "Capabilities.h"
#include <algorithm>

using std::find;

const string Capabilities::NEEDS_EXECUTION_TOOL_ = "needsExecutionTool";
const string Capabilities::CAN_HANDLE_VARIABLE_COMMUNICATION_STEP_SIZE_ = "canHandleVariableCommunicationStepSize";
const string Capabilities::CAN_INTERPOLATE_INPUTS_ = "canInterpolateInputs";
const string Capabilities::CAN_RUN_ASYNCHRONOUSLY_ = "canRunAsynchronously";
const string Capabilities::CAN_BE_INSTANTIATED_ONLY_ONCE_PER_PROCESS_ = "canBeInstantiatedOnlyOncePerProcess";
const string Capabilities::CAN_NOT_USE_MEMORY_MANAGMENT_FUNCTION_ = "canNotUseMemoryManagmentFunction";
const string Capabilities::CAN_GET_AND_SET_FMU_STATE_ = "canGetAndSetFmuState";
const string Capabilities::CAN_SERIALIZE_FMU_STATE_ = "canSerializeFmuState";
const string Capabilities::PROVIDES_DIRECTIONAL_DERIVATIVE_ = "providesDirectionalDerivative";

Capabilities::Capabilities(): capabilities_() {}

bool Capabilities::Check(string capability){
	return find(capabilities_.begin(), capabilities_.end(), capability) != capabilities_.end();
}

void Capabilities::Add(string capability){
	capabilities_.push_back(capability);
}
