#ifndef MODELDESCRIPTION_V2_STRINGTYPE_H_
#define MODELDESCRIPTION_V2_STRINGTYPE_H_

#include <string>
#include "SimpleType.h"

using std::string;


class StringType : public SimpleType {
private:
	string declared_type_;
	string start_;
public:
	StringType();
	void declared_type(string new_declared_type);
	string declared_type();
	void start(string start);
	string start();
};


#endif
