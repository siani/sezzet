#ifndef MODELDESCRIPTION_V2_CAPABILITIES_H_
#define MODELDESCRIPTION_V2_CAPABILITIES_H_

#include <string>
#include <vector>

using std::string;
using std::vector;

class Capabilities{
private:	
	vector<string> capabilities_;
public:
	
	const static string NEEDS_EXECUTION_TOOL_;
	const static string CAN_HANDLE_VARIABLE_COMMUNICATION_STEP_SIZE_;
	const static string CAN_INTERPOLATE_INPUTS_;
	const static string CAN_RUN_ASYNCHRONOUSLY_;
	const static string CAN_BE_INSTANTIATED_ONLY_ONCE_PER_PROCESS_;
	const static string CAN_NOT_USE_MEMORY_MANAGMENT_FUNCTION_;
	const static string CAN_GET_AND_SET_FMU_STATE_;
	const static string CAN_SERIALIZE_FMU_STATE_;
	const static string PROVIDES_DIRECTIONAL_DERIVATIVE_;

    Capabilities();
	bool Check(string capability);
	void Add(string capability);
};


#endif
