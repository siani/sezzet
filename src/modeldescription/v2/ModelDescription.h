#pragma once

#include <string>
#include <vector>
#include "Unit.h"
#include "SimpleType.h"
#include "Category.h"
#include "DefaultExperiment.h"
#include "Tool.h"
#include "ScalarVariable.h"
#include "ModelStructure.h"
#include "ModelExchange.h"
#include "CoSimulation.h"
#include <map>

using std::string;
using std::vector;

class ModelDescription {
private:
	string fmi_version_;
	string model_name_;
	string guid_;
	string description_;
	string author_;
	string version_;
	string copyright_;
	string license_;
	string generation_tool_;
	string generation_date_and_time_;
	string variable_naming_convention_;
	int number_of_event_indicators_;
	
	vector<Unit> unit_definitions_;
	vector<SimpleType> type_definitions_;
	vector<Category> log_categories_;
	DefaultExperiment default_experiment_;
	vector<Tool> vendor_annotations_;
	ModelStructure model_structure_;
	ModelExchange model_exchange_;
	CoSimulation co_simulation_;
	std::map<string, shared_ptr<ScalarVariable>> model_variables_;
	
public:
	ModelDescription();
	virtual ~ModelDescription();
	virtual void number_of_event_indicators(int new_number_of_event_indicators);
	virtual int number_of_event_indicators();
	virtual void variable_naming_convention(string new_variable_naming_convention);
	virtual string variable_naming_convention();
	virtual void generation_date_and_time(string new_generation_date_and_time);
	virtual string generation_date_and_time();
	virtual void generation_tool(string new_generation_tool);
	virtual string generation_tool();
	virtual void license(string new_license);
	virtual string license();
	virtual void copyright(string new_copyright);
	virtual string copyright();
	virtual void version(string new_version);
	virtual string version();
	virtual void author(string new_author);
	virtual string author();
	virtual void description(string new_description);
	virtual string description();
	virtual void guid(string new_guid);
	virtual string GetGuID();
	virtual void model_name(string new_model_name);
	virtual string model_name();
	virtual void fmi_version(string new_fmi_version);
	virtual string fmi_version();
	vector<unsigned> GetValueReferences(vector<std::basic_string<char>>& variable_names);
	virtual int GetValueReference(const string& variable_name);
	virtual vector<Unit> unit_definitions();
	virtual void AddUnitDefinition(Unit unit_definition);
	virtual vector<SimpleType> type_definitions();
	virtual void AddTypeDefinition(SimpleType type_definition);
	virtual vector<Category> log_categories();
	virtual void AddLogCategory(Category log_category);
	virtual DefaultExperiment default_experiment();
	virtual void default_experiment(DefaultExperiment new_default_experiment);
	virtual vector<Tool> vendor_annotations();
	virtual void AddVendorAnnotations(Tool vendor_annotation);
	virtual void AddModelVariable(ScalarVariable model_variable);
	virtual ModelStructure model_structure();
	virtual void model_structure(ModelStructure new_model_structure);
	virtual ModelExchange model_exchange();
	virtual void model_exchange(ModelExchange new_model_exchange);
	virtual CoSimulation GetCoSimulation();
	virtual string GetModelIdentifier();
	virtual void co_simulation(CoSimulation&& new_co_simulation);
	virtual std::map<string, shared_ptr<ScalarVariable>> model_variables();

	virtual shared_ptr<Capabilities> Capabilities();
};
