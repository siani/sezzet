#include "CoSimulation.h"

CoSimulation::CoSimulation() {
	capabilities_ = std::make_shared<Capabilities>();
}

CoSimulation::~CoSimulation() {}

std::basic_string<char> CoSimulation::GetModelIdentifier() { return model_identifier_; }

void CoSimulation::model_identifier(string new_model_identifier) { model_identifier_ = new_model_identifier; }

int CoSimulation::max_output_derivative_order() { return max_output_derivative_order_; }

void CoSimulation::max_output_derivative_order(int new_max_output_derivative_order) { max_output_derivative_order_ = new_max_output_derivative_order; }

shared_ptr<Capabilities> CoSimulation::GetCapabilities() {
	return capabilities_;
}

void CoSimulation::AddSourceFile(SourceFile source_file) { source_files_.push_back(source_file); }

vector<SourceFile> CoSimulation::source_files() { return source_files_; }