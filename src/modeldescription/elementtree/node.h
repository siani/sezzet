#pragma once
#include <unordered_map>
#include <vector>
#include <memory>

using std::unordered_map;
using std::string;
using std::vector;
using std::shared_ptr;

class Node{
	unordered_map<string, string> attributes;
	string name;
	vector<shared_ptr<Node>> children;

public:
	vector<shared_ptr<Node>> GetChildren();
	void SetName(string new_name);
	string GetName();
	unordered_map<string, string> GetAttributes();
	void AddAttribute(string attribute_name, string value);
	void AddChild(shared_ptr<Node>& child);
};
