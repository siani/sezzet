#include "Node.h"
#include <iostream>

using std::make_shared;

unordered_map<string, string> Node::GetAttributes() { return attributes; }
string Node::GetName(){return name;}
void Node::SetName(string new_name){name = new_name;}
vector<shared_ptr<Node>> Node::GetChildren(){return children;}


void Node::AddAttribute(string attribute_name, string value) {
	attributes.insert(std::pair<string, string>(attribute_name, value));
}

void Node::AddChild(shared_ptr<Node>& child){
	children.push_back(child);
}
