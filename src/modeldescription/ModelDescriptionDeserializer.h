#pragma once

#include <boost/property_tree/ptree_fwd.hpp>
#include "v2/ModelDescription.h"
#include <functional>
#include <memory>
#include "elementtree/Node.h"

using std::string;
using std::function;
using boost::property_tree::ptree;

typedef shared_ptr<ptree> PtreePointer;
typedef void(*FillerFunction)(ModelDescription&, shared_ptr<Node>&);

namespace ModelDescriptionDeserializer{
	shared_ptr<ModelDescription> AsV2(string xml_file_path);
};
